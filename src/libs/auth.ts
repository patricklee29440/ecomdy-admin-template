const TOKEN_KEY : string = 'id_token'

export default {
  setToken(token: string) {
    console.log(token)
    return localStorage.setItem(TOKEN_KEY, token);
  },

  getToken() {
    return localStorage.getItem(TOKEN_KEY) || '';
  },

  clearToken() {
    return localStorage.removeItem(TOKEN_KEY);
  },
}