import * as Vue from 'vue';
// axios
import axios from 'axios';
console.log(import.meta.env)

const axiosIns = axios.create({
  // You can add your headers here
  // ================================
  baseURL: import.meta.env.VITE_APP_SERVER_API,
  // baseURL: 'https://bd99-116-105-76-109.ap.ngrok.io',
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: `Bearer 1234`,
    timezone: '+7',
  },
});

axiosIns.interceptors.request.use(
  config => {
    // Get token from localStorage
    const accessToken = '123'

    // If token is present add it to request's Authorization Header
    if (accessToken) {
    // eslint-disable-next-line no-param-reassign
      config.headers.Authorization = `Bearer ${accessToken}`
    }
    return config
  },
  error => {
    throw (error.response)
  },
)

// throw error when call aixos error
axiosIns.interceptors.response.use(null, error => {
  throw (error.response)
});

// Vue.prototype.$http = axiosIns;

export default axiosIns;