import request from '@/libs/axios';

export default {
  login(params: any) {
    return request.post('/api/users/login', params);
  },
}