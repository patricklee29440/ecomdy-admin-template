import Vuex from 'vuex'
import app from './app';
import auth from './auth';

const store = new Vuex.Store({
  modules: {
    app,
    auth,
  }
});

export default store;