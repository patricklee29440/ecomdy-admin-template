export default {
  state: () => ({
    layout: {
      darkMode: false,
    },
  }),

  mutations: {
    setLayout(state: any, config: any) {
      state.layout = {
        ...state.layout,
        ...config,
      };
    },
  },

  getters: {
    modeTheme(state: any) {
      return state.layout.darkMode;
    }
  },

  actions: {},
}