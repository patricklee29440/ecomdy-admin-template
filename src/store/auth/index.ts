import AuthService from '@/services/auth';
import auth from '@/libs/auth';
import { getResponse } from '@/utils/response';

export default {
  namespaced: true,
  state: () => ({
    auth: {
      loading: false,
      isLogin: false,
      tokenId: '',
    },
  }),

  mutations: {
    setAuth(state: any, data: any) {
      state.auth = {
        ...state.auth,
        ...data,
      };
    },

    logOut(state: any) {
      state.auth = {
        ...state.auth,
        isLogin: false,
      };
    },
  },

  getters: {
    isLogin(state: any) {
      return state.auth.isLogin;
    }
  },

  actions: {
    async login({ commit }: any, user: any) {
      commit('setAuth', {
        loading: true,
      });

      try {
        const { data } = await AuthService.login(user);
        const response = getResponse(data);
        auth.setToken(response.id_token);

        commit('setAuth', {
          loading: false,
          isLogin: true,
          tokenId: response.id_token,
        });
      } catch (error) {
        commit('setAuth', {
          loading: false,
          isLogin: false,
          tokenId: '',
        });
      }
    }
  },
}