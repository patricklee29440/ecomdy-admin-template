import type { App } from 'vue';
import { createI18n } from 'vue-i18n';
import enUS from './lang/en-US.json';
import viVN from './lang/vi-VN.json';

const localeOption = {
  locale: 'en_US', // set locale
  fallbackLocale: 'en_US', // set fallback locale
  messages: {
    en_US: enUS,
    vi_VN: viVN,
  }
}

export type LocaleType = keyof typeof localeMap;

export const localeMap = {
  en_US: 'en_US',
  vi_VN: 'vi_VN',
} as const;

export const localeList = [
  {
    lang: localeMap.en_US,
    label: 'English',
    icon: '🇺🇸',
  },
  {
    lang: localeMap.vi_VN,
    label: 'Vietnamese',
    icon: '🇻🇳',
  },
] as const;

export const i18n = createI18n(localeOption);

export function setupI18n(app: App) {
  app.use(i18n);
};