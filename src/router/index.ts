import { createRouter, createWebHistory } from 'vue-router';
import Login from '@/views/login/index.vue';
import Dashboard from '@/views/dashboard/index.vue';
import { menuList } from '@/components/menu/menu';

const defaultRoutes = [
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/',
        name: 'Dashboard',
        component: Dashboard
    },
];

const dynamicRoutes = menuList.map(menu => {
    return {
        path: menu.route,
        name: menu.name,
        component: menu.component,
    }
});

const routes = [
    ...dynamicRoutes,
    ...defaultRoutes,
];

const router = createRouter({ history: createWebHistory(), routes  })
export default router;