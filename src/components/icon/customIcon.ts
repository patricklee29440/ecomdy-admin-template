import { h } from 'vue';
import './icon.scss';

const CustomIcon = (props: any) => {
  return h(
    "img",
    {
      ...props,
      class: `anticon custom-icon ${props.class}`,
    },
  )
};

CustomIcon.props = ['src'];

export default CustomIcon;