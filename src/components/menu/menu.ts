import User from '@/views/user/index.vue';
import Accounts from '@/views/account/index.vue';
import Product from '@/views/product/index.vue';
import {
  UserOutlined,
  EuroOutlined,
} from '@ant-design/icons-vue';

import CustomIcon from '@/components/icon/customIcon';
import vueIcon from "@/assets/vue.svg";

const VueIcon = CustomIcon({
  src: vueIcon,
});

const menuList: Array<Object> = [
  {
    name: 'Users',
    label: 'Users',
    route: '/users',
    icon: UserOutlined,
    component: User,
  },
  {
    name: 'Accounts',
    label: 'Accounts',
    route: '/accounts',
    icon: EuroOutlined,
    component: Accounts,
  },
  {
    name: 'Product',
    label: 'Product',
    route: '/product',
    icon: VueIcon,
    component: Product,
  },
];

export {
  menuList
};