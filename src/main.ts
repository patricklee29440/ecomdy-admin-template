import { createApp } from "vue";
import "./style.scss";
import 'ant-design-vue/dist/antd.variable.min.css';
import App from "./App.vue";
import Antd from "ant-design-vue";
import store from './store';
import { setupI18n } from "./locales";
import router from "./router";

const app = createApp(App);
app.use(store);
app.use(router);
setupI18n(app);
app.use(Antd).mount("#app");
